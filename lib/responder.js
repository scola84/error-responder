module.exports = () => {
  return (error, request, response, next) => {
    response.status(error.code || 500);

    if (error.code) {
      response.json({
        code: error.code,
        reason: error.code === 400 && error.origin ?
          error.origin.message : undefined,
        message: error.code === 500 ?
          'Internal server error' : error.message
      });
    }

    response.end();
    response.destroy();
  };
};
